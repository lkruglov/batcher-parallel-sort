#include "common.h"
#include "command_line_parser.h"
#include "io_processing.h"
#include "validation.h"
#include "parallel_sorter.h"

#include <mpi.h>

#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>
#include <cassert>

namespace
{

void PrintHelp()
{
	std::cout << "Usage: mpirun -np <num_processes> <prog> --input=<path> --statistics-output==<path> [--output=<path>] [--root=<integer>] [--disable-validation]" << std::endl;
}

void ValidateCommandLineArgs(const CommandLineParser::Options &options, int commSize)
{
	try
	{
		CHECK(options.root >= 0, "Invalid root, must be >= 0");
		CHECK(options.root < commSize, "Invalid root, must be less than number of processes");
		CHECK(!options.inputFilePath.empty(), "Empty input file path");
		CHECK(!options.statisticsFilePath.empty(), "Empty statistics file path");
	}
	catch (const std::runtime_error &exc)
	{
		PrintHelp();
		throw;
	}
}

} // namespace

int main(int argc, char **argv)
{
	MPI_CHECK_SUCCEEDED_RETURN(MPI_Init(&argc, &argv));

	try
	{
		TimeStatistics timeStatistics;
		timeStatistics.startTime = MPI_Wtime();

		rank_t rank = -1;
		int commSize = -1;

		MPI_CHECK_SUCCEEDED(MPI_Comm_rank(MPI_COMM_WORLD, &rank));	
		MPI_CHECK_SUCCEEDED(MPI_Comm_size(MPI_COMM_WORLD, &commSize));	

		CommandLineParser cmdLineParser(argc, argv);
		auto options = cmdLineParser.Parse();
		ValidateCommandLineArgs(options, commSize);

		std::shared_ptr<validation::IValidator> validator;
		if (options.disableValidation)
		{
			validator = std::make_shared<validation::NoValidator>();
		}
		else
		{
			validator = std::make_shared<validation::Validator>();
		}

		validation::ArrayValidationInfo initialArrayValidationInfoForRoot;
		std::vector<unsigned int> fullArrayForRoot;

		if (rank == options.root)
		{
			timeStatistics.readStartTime = MPI_Wtime();
			ReadArray(fullArrayForRoot, options.inputFilePath);
			timeStatistics.readEndTime = MPI_Wtime();

			validation::FillInArrayValidationInfo(fullArrayForRoot, commSize, initialArrayValidationInfoForRoot);
		}

		timeStatistics.sortStartTime = MPI_Wtime();
		auto sorter = OddEvenMergeParallelRootSorter(options.root, MPI_COMM_WORLD, fullArrayForRoot, validator);
		sorter.Sort();
		timeStatistics.sortEndTime = MPI_Wtime();
		
		if (rank == options.root && !options.outputFilePath.empty())
		{
			WriteArray(fullArrayForRoot, options.outputFilePath);
		}

		timeStatistics.endTime = MPI_Wtime();
		if (rank == options.root)
		{
			validation::ArrayValidationInfo sortedArrayValidationInfo;
			validation::FillInArrayValidationInfo(fullArrayForRoot, commSize, sortedArrayValidationInfo);
			validator->ValidateControlSums(initialArrayValidationInfoForRoot, sortedArrayValidationInfo);

			WriteStatistics(commSize, fullArrayForRoot.size(), sorter.GetTactToComparatorsMap(),
				initialArrayValidationInfoForRoot, sortedArrayValidationInfo,
				timeStatistics, options.statisticsFilePath);		
		}

	}
	catch (const MPIException &exc)
	{
		std::stringstream msg;
		msg << "MPI Error: " << exc.what() << std::endl;
		std::cerr << msg.str();
		MPI_Abort(MPI_COMM_WORLD, 1);
		return 1;
	}
	catch (const std::exception &exc)
	{
		std::stringstream msg;
		msg << "Error: " << exc.what() << std::endl;
		std::cerr << msg.str();
		MPI_Abort(MPI_COMM_WORLD, 1);
		return 1;
	}
	catch (...)
	{
		std::stringstream msg;
		msg << "Unknown error." << std::endl;
		std::cerr << msg.str();
		MPI_Abort(MPI_COMM_WORLD, 1);
		return 1;
	}

	MPI_CHECK_SUCCEEDED_RETURN(MPI_Finalize());
	return 0;
}
