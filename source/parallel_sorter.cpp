#include "parallel_sorter.h"
#include "common.h"
#include "network_generation.h"
#include "array_info_helpers.h"
#include "validation.h"
#include "merge_arrays.h"
#include "heap_sort.h"

#include <vector>
#include <memory>
#include <cassert>

#include <mpi.h>

OddEvenMergeParallelRootSorter::OddEvenMergeParallelRootSorter(
		rank_t root,
		MPI_Comm communicator,
		std::vector<unsigned int> &fullArrayForRoot,
		const std::shared_ptr<validation::IValidator> &validator) : 
	m_root(root),
	m_rank(-1),
	m_communicator(communicator),
	m_commSize(0),
	m_initialArrayRef(fullArrayForRoot),
	m_fullArrayForRoot(fullArrayForRoot),
	m_elementsPerProcess(0),
	m_numberOfFakeZeroesForRoot(0),
	m_validator(validator)
{
	assert(m_root >= 0);

	MPI_CHECK_SUCCEEDED(MPI_Comm_rank(m_communicator, &m_rank));
	MPI_CHECK_SUCCEEDED(MPI_Comm_size(m_communicator, &m_commSize));
	
	m_tactToComparatorsMap = GenerateComparatorMap(m_commSize);
	m_perProcessSortingNetwork.reset(new PerProcessSortingNetwork(m_rank, m_tactToComparatorsMap));
}

void OddEvenMergeParallelRootSorter::Sort()
{
	InitializeArrayPart();
	HeapSort(m_arrayPart.begin(), m_arrayPart.end());

	rank_t partnerRank = -1;
	while (m_perProcessSortingNetwork->GetPartnerForNextTact(partnerRank))
	{
		if (partnerRank >= 0)
		{
			MPI_Request request;
			MPI_CHECK_SUCCEEDED(MPI_Isend(
				&m_arrayPart[0], m_arrayPart.size(), MPI_UNSIGNED,
				partnerRank, partnerRank, m_communicator, &request));

			std::vector<unsigned int> partnerArrayPart(m_elementsPerProcess);
			MPI_Status receiveStatus;
			MPI_CHECK_SUCCEEDED(MPI_Recv(
				&partnerArrayPart[0], m_elementsPerProcess, MPI_UNSIGNED,
				partnerRank, m_rank, m_communicator, &receiveStatus));

			MPI_Status sendWaitStatus;
			MPI_CHECK_SUCCEEDED(MPI_Wait(&request, &sendWaitStatus));

			PerformMerge(partnerRank, partnerArrayPart);

			m_validator->ValidateProcessorState(m_rank, m_arrayPart);
			m_validator->ValidateProcessorPairState(m_rank, m_arrayPart, partnerRank);
		}

		MPI_CHECK_SUCCEEDED(MPI_Barrier(m_communicator));
	}

	CollectResultArray();
}

void OddEvenMergeParallelRootSorter::InitializeArrayPart()
{
	if (IsRoot())
	{	
		const std::size_t initialArraySize = m_fullArrayForRoot.size();
		m_elementsPerProcess = GetNumberOfElementsPerProcess(initialArraySize, m_commSize);
		
		m_numberOfFakeZeroesForRoot = GetNumberOfFakeZeroes(initialArraySize, m_commSize);
		m_fullArrayForRoot.resize(initialArraySize + m_numberOfFakeZeroesForRoot, 0);
	}

	MPI_CHECK_SUCCEEDED(MPI_Bcast(&m_elementsPerProcess, 1, MPI_UNSIGNED_LONG, m_root, m_communicator));

	m_arrayPart.resize(m_elementsPerProcess);
	MPI_CHECK_SUCCEEDED(MPI_Scatter(&m_fullArrayForRoot[0], m_elementsPerProcess, MPI_UNSIGNED,
									&m_arrayPart[0], m_elementsPerProcess, MPI_UNSIGNED,
									m_root, m_communicator));
}

void OddEvenMergeParallelRootSorter::CollectResultArray()
{
	MPI_CHECK_SUCCEEDED(MPI_Gather(&m_arrayPart[0], m_elementsPerProcess, MPI_UNSIGNED,
									&m_fullArrayForRoot[0], m_elementsPerProcess, MPI_UNSIGNED,
									m_root, m_communicator));
	if (IsRoot())
	{
		m_fullArrayForRoot.erase(m_fullArrayForRoot.begin(), std::next(m_fullArrayForRoot.begin(), m_numberOfFakeZeroesForRoot));
		m_initialArrayRef.swap(m_fullArrayForRoot);
	}
}

void OddEvenMergeParallelRootSorter::PerformMerge(rank_t partnerRank, const std::vector<unsigned int> &partnerArrayPart)
{
	assert(m_rank != partnerRank);
	assert(m_arrayPart.size() == partnerArrayPart.size());

	if (m_rank > partnerRank)
	{
		MergeGetUpper(m_arrayPart, partnerArrayPart).swap(m_arrayPart);
	}
	else
	{
		MergeGetLower(m_arrayPart, partnerArrayPart).swap(m_arrayPart);
	}
}
