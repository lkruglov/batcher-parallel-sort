#include "merge_arrays.h"

#include <vector>
#include <functional>

namespace
{

template<class IteratorType>
std::vector<unsigned int> MergeWithCompareFunc(
		IteratorType sourceBegin, IteratorType sourceEnd,
		IteratorType partnerBegin, IteratorType partnerEnd,
		std::function<bool(int, int)> cmp,
		bool needResultReverse) 
{
	const std::size_t resultFullSize = std::distance(sourceBegin, sourceEnd);
	std::vector<unsigned int> result;
	result.reserve(resultFullSize);

	auto sourceIter = sourceBegin;
	auto partnerIter = partnerBegin;

	while (sourceIter != sourceEnd && result.size() != resultFullSize)
	{
		if (partnerIter == partnerEnd || cmp(*sourceIter, *partnerIter))
		{
			result.push_back(*sourceIter);
			++sourceIter;
		}
		else
		{
			result.push_back(*partnerIter);
			++partnerIter;
		}
	}

	if (needResultReverse)
	{
		return std::vector<unsigned int>(result.rbegin(), result.rend());
	}
	return result;
}

} // namespace

std::vector<unsigned int> MergeGetLower(const std::vector<unsigned int> &source,const std::vector<unsigned int> &partner)
{
	return MergeWithCompareFunc(
		source.begin(), source.end(),
		partner.begin(), partner.end(),
		std::less_equal<unsigned int>(),
		false);
}

std::vector<unsigned int> MergeGetUpper(const std::vector<unsigned int> &source,const std::vector<unsigned int> &partner)
{
	return MergeWithCompareFunc(
		source.rbegin(), source.rend(),
		partner.rbegin(), partner.rend(),
		std::greater_equal<unsigned int>(),
		true);
}
