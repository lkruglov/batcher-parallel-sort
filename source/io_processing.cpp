#include "io_processing.h"
#include "common.h"
#include "validation.h"

#include <string>
#include <vector>
#include <set>
#include <ostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <cassert>

namespace
{

void WriteSortingNetwork(int commSize, const OddEvenMergeSorter::TactToComparatorsMap &tactToCompMap, std::ostream &stream)
{
	stream << commSize << " 0 0" << std::endl;
	stream << tactToCompMap.size() << std::endl;

	std::stringstream comparatorStream;
	std::set<OddEvenMergeSorter::Comparator> comparatorSet;

	std::for_each(tactToCompMap.begin(), tactToCompMap.end(), 
		[&comparatorStream, &comparatorSet](const OddEvenMergeSorter::TactToComparatorsMap::value_type &tactInfo)
	{
		std::for_each(tactInfo.second.begin(), tactInfo.second.end(),
			[&comparatorStream, &comparatorSet](const OddEvenMergeSorter::Comparator &comparator)
		{
			comparatorStream << comparator.first << " " << comparator.second << " ";
			comparatorSet.insert(comparator);
		});
		comparatorStream << std::endl;
	});

	stream << comparatorSet.size() << std::endl;
	stream << std::endl;
	stream << comparatorStream.str() << std::endl;
}

void WriteGeneralArrayInfo(int commSize, std::size_t arraySize, std::ostream &stream)
{
	stream << arraySize << std::endl;
	stream << GetNumberOfElementsPerProcess(arraySize, commSize) << std::endl;
	stream << std::endl;
}

void WriteArrayValidationInfo(
	const validation::ArrayValidationInfo &initialArrayInfo,
	const validation::ArrayValidationInfo &sortedArayInfo,
	std::ostream &stream)
{
	stream << initialArrayInfo.sumFullArrayModPower32 << std::endl;
	stream << sortedArayInfo.sumFullArrayModPower32 << std::endl;
	stream << std::endl;

	{
		std::ostream_iterator<unsigned long long> iter(stream, " ");
		std::copy(initialArrayInfo.sumArrayPartsModPower32.begin(), initialArrayInfo.sumArrayPartsModPower32.end(), iter);
		stream << std::endl;
	}

	{
		std::ostream_iterator<unsigned long long> iter(stream, " ");
		std::copy(sortedArayInfo.sumArrayPartsModPower32.begin(), sortedArayInfo.sumArrayPartsModPower32.end(), iter);
		stream << std::endl;
	}

	stream << std::endl;
}

void WriteTimeStatistics(const TimeStatistics &timeStatistics, std::ostream &stream)
{
	stream << (timeStatistics.readEndTime - timeStatistics.readStartTime) << std::endl;
	stream << (timeStatistics.sortEndTime - timeStatistics.sortStartTime) << std::endl;
	stream << (timeStatistics.endTime - timeStatistics.startTime) << std::endl;
}

} // namespace

void ReadArray(std::vector<unsigned int> &resultArray, const std::string &filePath)
{
	std::ifstream stream(filePath);
	CHECK(stream.good(), "Cannot open input file");

	int arraySize = -1;
	stream >> arraySize;
	CHECK(arraySize >= 0, "Invalid array size, must be >= 0");

	std::vector<unsigned int> array;
	
	int element = 0;	
	while((stream >> element) && (arraySize > 0))
	{
		CHECK(element >= 0, "Non-negative elements expected");
		array.push_back(element);
		--arraySize;
	}

	CHECK(arraySize == 0, "Not enough elements in array");
	resultArray.swap(array);
}

void WriteArray(const std::vector<unsigned int> &array, const std::string &filePath)
{
	std::ofstream stream(filePath);
	CHECK(stream.good(), "Cannot open output file");

	std::ostream_iterator<int> iter(stream, " ");
	std::copy(array.begin(), array.end(), iter);
}

void WriteStatistics(int commSize, std::size_t arraySize,
	const OddEvenMergeSorter::TactToComparatorsMap &tactToComparatorsMap,
	const validation::ArrayValidationInfo &initialArrayInfo,
	const validation::ArrayValidationInfo &sortedArayInfo,
	const TimeStatistics &timeStatistics,
	const std::string &filePath)
{
	std::ofstream stream(filePath);
	CHECK(stream.good(), "Cannot open statistics file");

	WriteSortingNetwork(commSize, tactToComparatorsMap, stream);
	WriteGeneralArrayInfo(commSize, arraySize, stream);
	WriteArrayValidationInfo(initialArrayInfo, sortedArayInfo, stream);
	WriteTimeStatistics(timeStatistics, stream);
}
