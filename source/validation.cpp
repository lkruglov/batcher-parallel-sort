#include "validation.h"
#include "common.h"
#include "array_info_helpers.h"

#include <vector>
#include <iterator>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>

#include <mpi.h>

namespace validation
{

namespace
{

const unsigned int Power16 = static_cast<unsigned long long>(1) << 16;
const unsigned long long Power32 = static_cast<unsigned long long>(1) << 32;

unsigned long long GetArrayPartialSum(std::vector<unsigned int>::const_iterator first, std::vector<unsigned int>::const_iterator last)
{
	const unsigned long long sum =
		std::accumulate(first, last, static_cast<unsigned long long>(0));
	return sum % Power32;
}

unsigned long long GetArrayPartialSum(const std::vector<unsigned int> &array)
{
	return GetArrayPartialSum(array.begin(), array.end());
}

std::vector<unsigned long long> GetArrayPartPartialSums(const std::vector<unsigned int> &fullArray, int commSize)
{
	std::vector<unsigned long long> result;
	const std::size_t elementsPerProcess = GetNumberOfElementsPerProcess(fullArray.size(), commSize);
	for (auto it = fullArray.begin(); it < fullArray.end(); std::advance(it, elementsPerProcess))
	{
		auto partEnd = std::next(it, elementsPerProcess);
		if (partEnd >= fullArray.end())
		{
			partEnd = fullArray.end();
		}
		result.push_back(GetArrayPartialSum(it, partEnd));
	}
	return result;
}

std::vector<unsigned int> GetElementsModPower16ValidationArray(const std::vector<unsigned int> &array)
{	
	std::vector<unsigned int> result(Power16, 0);
	
	std::for_each(array.begin(), array.end(), [&result, Power16](int value)
	{
		const unsigned int position = value % Power16;
		result[position]++;
	});

	return result;
}

} // namespace


validation::IValidator::~IValidator()
{
}


void FillInArrayValidationInfo(const std::vector<unsigned int> &fullArray, int commSize, ArrayValidationInfo &arrayInfo)
{
	arrayInfo.sumFullArrayModPower32 = GetArrayPartialSum(fullArray);
	arrayInfo.sumArrayPartsModPower32 = GetArrayPartPartialSums(fullArray, commSize);
	arrayInfo.numberOfElementsModPower16 = GetElementsModPower16ValidationArray(fullArray);
}

void Validator::ValidateProcessorState(rank_t rank, const std::vector<unsigned int> &arrayPart)
{
	assert(rank >= 0);
	CHECK(!arrayPart.empty(),
		"Validation failed: array for processor " + std::to_string(rank) + " is empty");
	CHECK(std::is_sorted(arrayPart.begin(), arrayPart.end()),
		"Validation failed: array for processor " + std::to_string(rank) + " is not sorted");
}

void Validator::ValidateProcessorPairState(rank_t ownRank, const std::vector<unsigned int> &arrayPart, rank_t partnerRank)
{
	assert(ownRank >= 0);
	assert(partnerRank >= 0);
	assert(ownRank != partnerRank);
	assert(!arrayPart.empty());

	const auto elementToSend =
		ownRank > partnerRank ? *(arrayPart.begin()) : *(--arrayPart.end());

	MPI_Request request;
	MPI_CHECK_SUCCEEDED(MPI_Isend(
		&elementToSend, 1, MPI_UNSIGNED,
		partnerRank, partnerRank, MPI_COMM_WORLD, &request));

	unsigned int receivedElement = 0;

	MPI_Status receiveStatus;
	MPI_CHECK_SUCCEEDED(MPI_Recv(
		&receivedElement, 1, MPI_UNSIGNED,
		partnerRank, ownRank, MPI_COMM_WORLD, &receiveStatus));

	MPI_Status sendWaitStatus;
	MPI_CHECK_SUCCEEDED(MPI_Wait(&request, &sendWaitStatus));

	std::stringstream pairValidationFailedMsg;
	pairValidationFailedMsg
		<< "Validation failed: arrays at processors ("
		<< ownRank << ", " << partnerRank << ") are merged incorrectly. Culprit values: "
		<< elementToSend << " and " << receivedElement;

	if (ownRank > partnerRank)
	{
		CHECK(elementToSend >= receivedElement, pairValidationFailedMsg.str());
	}
	else
	{
		CHECK(elementToSend <= receivedElement, pairValidationFailedMsg.str());
	}
}

void Validator::ValidateControlSums(const ArrayValidationInfo &initialArrayInfo, const ArrayValidationInfo &sortedArayInfo)
{
	CHECK(initialArrayInfo.sumFullArrayModPower32 == sortedArayInfo.sumFullArrayModPower32,
		"Post-sort validation failed: 'full array sum mod 2^32' check failed");

	const std::vector<unsigned long long> &initialSumArrayParts = initialArrayInfo.sumArrayPartsModPower32;
	const std::vector<unsigned long long> &sortedSumArrayParts = sortedArayInfo.sumArrayPartsModPower32;
	CHECK(initialSumArrayParts.size() == sortedSumArrayParts.size(),
		"Post-sort validation failed: Mismatched initial and array part sizes");

	const std::vector<unsigned int> &initialNumberOfElements = initialArrayInfo.numberOfElementsModPower16;
	const std::vector<unsigned int> &sortedNumberOfElements = sortedArayInfo.numberOfElementsModPower16;
	CHECK(initialNumberOfElements.size() == Power16,
		"Post-sort validation failed: Invalid 'number of elements mod 2^16' initial array size");
	CHECK(sortedNumberOfElements.size() == Power16,
		"Post-sort validation failed: Invalid 'number of elements mod 2^16' sorted array size");

	for (std::size_t i = 0; i < Power16; ++i)
	{
		CHECK(initialNumberOfElements[0] == sortedNumberOfElements[0],
			"Post-sort validation failed: 'number of elements mod 2^16' check not passed");
	}
}

} // namespace validation
