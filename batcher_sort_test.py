import sys
import argparse
import os
import subprocess
import shutil
import random
import tempfile
import time

class ConsoleColors:
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	BOLD = '\033[1m'
	ENDC = '\033[0m'

	def Disable(self):
		self.OKGREEN = ''
		self.WARNING = ''
		self.FAIL = ''
		self.BOLD = ''
		self.ENDC = ''

def Colored(color, *args):
	result = OutputColors.BOLD + color
	for arg in args:
		result += str(arg)
	return result + OutputColors.ENDC

def OK(*args):
	return Colored(OutputColors.OKGREEN, *args)

def ERROR(*args):
	return Colored(OutputColors.FAIL, *args)

def WARNING(*args):
	return Colored(OutputColors.WARNING, *args)

OutputColors = ConsoleColors()

RunHeader       = OK('[ RUN      ]')
OkHeader        = OK('[       OK ]')
FailedHeader = ERROR('[  FAILED  ]')

ArrayDataFileName = 'array.txt'
SortedArrayFileName = 'sorted.txt'
StatisticsFileName = 'statistics.txt'

def ReadArray(filePath):
	with open(filePath, 'r') as f:
		contents = f.read().split()
		return [int(v) for v in contents]

def ValidateOutputArray(sortedArrayFilePath, initialNumberOfElements):
	sortedArray = ReadArray(sortedArrayFilePath)
	if len(sortedArray) != initialNumberOfElements:
		print ERROR('Different number of elements in sorted array')
		return False

	if not all((sortedArray[i] <= sortedArray[i+1]) for i in xrange(len(sortedArray)-1)):
		print ERROR('Result array is not sorted')
		return False

	return True

def CreateRandomTestData(numberOfElements, maxArrayElement, arrayDataFilePath):
	with open(arrayDataFilePath, 'w+') as inputFile:
		inputFile.write(str(numberOfElements) + '\n\n')
		for r in xrange(numberOfElements):
			value = 0 if maxArrayElement == 0 else random.randint(0, maxArrayElement)
			inputFile.write(str(value) + ' ')
		inputFile.write('\n')

def RunRandomTest(pathToExecutable, numProcessors, numElements, maxArrayElement, storeResults, testName):
	result = False

	try:
		tempDir = tempfile.mkdtemp(prefix='sort_data_', dir='./')

		arrayDataFilePath = os.path.join(tempDir, ArrayDataFileName)
		sortedArrayFilePath = os.path.join(tempDir, SortedArrayFileName)
		statisticsFilePath = os.path.join(tempDir, StatisticsFileName)

		numberOfPocessors = numProcessors if numProcessors else random.randint(1, 32)
		rootProcess = 0 if (numberOfPocessors == 1) else random.randint(0, numberOfPocessors - 1)
		numberOfElements = numElements if numElements else random.randint(0, 10000)
		
		CreateRandomTestData(numberOfElements, maxArrayElement, arrayDataFilePath)

		print RunHeader, testName + ':', \
				numberOfPocessors, 'processors,', \
				numberOfElements, 'elements,', 'root =', rootProcess

		startTime = time.time()
		p = subprocess.Popen(
			['mpirun', '-np', str(numberOfPocessors), pathToExecutable,
				'--input=' + arrayDataFilePath,
				'--output=' + sortedArrayFilePath,
				'--statistics-output=' + statisticsFilePath],
			stdout = subprocess.PIPE)
		stdout, stderr = p.communicate()
		sortTime = time.time() - startTime

		retCode = p.returncode
		if retCode != 0:
			if stderr:
				print stderr
			print ERROR('Returned non-zero code =', retCode)
			print FailedHeader, testName
			result = False
		else:
			validationResult = ValidateOutputArray(sortedArrayFilePath, numberOfElements)
			if not validationResult:
				print FailedHeader, testName
				result = False
			elif not os.path.exists(statisticsFilePath):
				print FailedHeader, testName + ': statistics file not found'
				result = False
			else:
				print OkHeader, testName + ':', sortTime, 'sec', \
						WARNING('(results saved to ' + tempDir + ')') if storeResults else ''
				result = True

		if not storeResults:
			shutil.rmtree(tempDir)

	except Exception as ex:
		print ERROR('Exception occured: ', ex)
		print FailedHeader, testName
		
	return result

def main():
	try:
		parser = argparse.ArgumentParser()
		parser.add_argument('-p', '--program', action='store', \
			help='path sorting program executable')
		parser.add_argument('-n', '--tests-number', type=int, default=5, \
			dest='testsNumber', help='numer of tests to run (defaults to 5)')
		parser.add_argument('-s', '--store-results', default=False, action='store_true', \
			dest='storeResults', help='keep test run results')
		parser.add_argument('-np', '--num-processors', type=int,
			dest='numProcessors', help='number of processors')
		parser.add_argument('-ne', '--num-elements', type=int,
			dest='numElements', help='number of elements in array')
		parser.add_argument('-me', '--max-array-element', type=int, default=100000, \
			dest='maxArrayElement', help='max possible unsigned int value for array data')

		args = parser.parse_args()
		if not args.program:
			print 'Path to program must be specified'
			return 1
		if args.numProcessors and args.numProcessors < 1:
			print 'Number of processors must be >= 1'
			return 1
		if args.numElements and args.numElements < 0:
			print 'Number of elements must be non-negative'
			return 1
		if args.maxArrayElement < 0:
			print 'Max value must be non-negative'
			return 1

		if not sys.stdout.isatty():
			OutputColors.Disable()

		for testIndex in xrange(args.testsNumber):
			RunRandomTest(args.program,
							args.numProcessors, args.numElements, args.maxArrayElement,
							args.storeResults, 'Test ' + str(testIndex + 1))

	except Exception as ex:
		print 'Exception in main:', ex
		return 1

	return 0


if __name__ == "__main__":
    sys.exit(main())