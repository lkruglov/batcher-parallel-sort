#ifndef __ARRAY_INFO_HELPERS_H__
#define __ARRAY_INFO_HELPERS_H__

#include <cstddef>
#include <cassert>

inline std::size_t GetNumberOfFakeZeroes(std::size_t arraySize, int commSize)
{
	assert(arraySize >= 0);
	assert(commSize > 0);
	const std::size_t commSizeUnsigned = static_cast<std::size_t>(commSize);
	const std::size_t elementsPerProcessRest = arraySize % commSizeUnsigned;
	return commSizeUnsigned - elementsPerProcessRest;
}

inline std::size_t GetNumberOfElementsPerProcess(std::size_t arraySize, int commSize)
{
	assert(commSize > 0);
	const std::size_t commSizeUnsigned = static_cast<std::size_t>(commSize);
	const std::size_t numberOfFakeZeroes = GetNumberOfFakeZeroes(arraySize, commSize);
	const std::size_t updatedArraySize = arraySize + numberOfFakeZeroes;
	return updatedArraySize / commSizeUnsigned;
}

#endif // __ARRAY_INFO_HELPERS_H__
