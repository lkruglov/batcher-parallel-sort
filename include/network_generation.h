#ifndef __NETWORK_GENERATION_H__
#define __NETWORK_GENERATION_H__

#include "odd_even_merge_sorter.h"

#include <vector>
#include <map>
#include <algorithm>

inline OddEvenMergeSorter::TactToComparatorsMap GenerateComparatorMap(int commSize)
{
	std::vector<int> dummyVector(commSize);	
	OddEvenMergeSorter sorter(dummyVector);
	return sorter.Sort();
}

typedef std::map<int, int> TactToPartnerMap;

inline TactToPartnerMap GetPartnerForEachTact(int processorNum, const OddEvenMergeSorter::TactToComparatorsMap &tactToCompMap)
{
	TactToPartnerMap result;

	std::for_each(tactToCompMap.begin(), tactToCompMap.end(),
		[processorNum, &result](const OddEvenMergeSorter::TactToComparatorsMap::value_type &elem)
	{
		const auto tact = elem.first;
		const auto &comparators = elem.second;

		for (auto it = comparators.begin(); it != comparators.end(); ++it)
		{
			const OddEvenMergeSorter::Comparator &comparator = *it;
			if (comparator.first == processorNum)
			{
				result.insert(std::make_pair(tact, comparator.second));
				break;
			}
			if (comparator.second == processorNum)
			{
				result.insert(std::make_pair(tact, comparator.first));
				break;
			}
		}

		// Processor is stale at this tact
		const int invalidProcessNumber = -1;
		result.insert(std::make_pair(tact, invalidProcessNumber));
	});

	return result;
}

#endif //__NETWORK_GENERATION_H__
