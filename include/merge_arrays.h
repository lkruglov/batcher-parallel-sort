#ifndef __MERGE_ARRAYS_H__
#define __MERGE_ARRAYS_H__

#include <vector>

std::vector<unsigned int> MergeGetLower(const std::vector<unsigned int> &source, const std::vector<unsigned int> &partner);
std::vector<unsigned int> MergeGetUpper(const std::vector<unsigned int> &source, const std::vector<unsigned int> &partner);

#endif //__MERGE_ARRAYS_H__
