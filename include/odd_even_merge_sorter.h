#ifndef __ODD_EVEN_MERGE_SORTER_H__
#define __ODD_EVEN_MERGE_SORTER_H__

#include <vector>
#include <map>
#include <cmath>
#include <algorithm>

typedef int ValueType;

//todo: use template
class OddEvenMergeSorter
{

public:
	typedef std::pair<int, int> Comparator;
	typedef std::vector<Comparator> ComparatorList;
	typedef std::map<int, ComparatorList> TactToComparatorsMap;

private:
	typedef std::map<int, ComparatorList, std::greater<int> > ComparatorMapForMerge;
	typedef std::map<int, ComparatorMapForMerge, std::greater<int> > ComparatorMapForSort;

public:
	OddEvenMergeSorter(std::vector<ValueType> &vec): m_vec(vec)
	{
	}

	TactToComparatorsMap Sort()
	{
		const std::size_t realLen = m_vec.size();
		std::size_t ceilPowerOf2Len = realLen;
		if ((realLen & (realLen - 1)) != 0)
		{
			// Vector length is not power of 2. Sort as if there are power of 2 elements.
			// Non-existing elements are ignored: that is correct because
			// larger elements are always sent to larger indexed positions.
			ceilPowerOf2Len = static_cast<std::size_t>(std::pow(2, std::ceil(std::log((double)realLen)/std::log(2.0))));
		}
		ComparatorMapForSort comparatorMapForSort;
		OddEvenMergeSort(0, ceilPowerOf2Len, comparatorMapForSort, 0);
		return CreateTactToComparatorsMap(comparatorMapForSort);
	}

private:
	void OddEvenMerge(std::size_t startPos, std::size_t len, std::size_t compareDistance, ComparatorMapForMerge &comparatorMap, int mergeStep)
	{
		const std::size_t updatedDistance = compareDistance * 2;
		if (updatedDistance < len)
		{
			OddEvenMerge(startPos, len, updatedDistance, comparatorMap, mergeStep + 1);
			OddEvenMerge(startPos + compareDistance, len, updatedDistance, comparatorMap, mergeStep + 1);
			for (std::size_t i = startPos + compareDistance; i + compareDistance < startPos + len; i += updatedDistance)
			{
				if (i + compareDistance < m_vec.size())
				{
					InsertIntoComparatorMapForMerge(comparatorMap, mergeStep, Comparator(i, i + compareDistance));
					CompareSwap(m_vec[i], m_vec[i + compareDistance]);
				}
			}
		}
		else
		{
			if (startPos + compareDistance < m_vec.size())
			{
				InsertIntoComparatorMapForMerge(comparatorMap, mergeStep, Comparator(startPos, startPos + compareDistance));
				CompareSwap(m_vec[startPos], m_vec[startPos + compareDistance]);
			}
		}
	}

	void OddEvenMergeSort(std::size_t startPos, std::size_t len, ComparatorMapForSort &comparatorMapForSort, int sortStep)
	{
		if (len > 1)
		{
			const std::size_t half = len / 2;
			OddEvenMergeSort(startPos, half, comparatorMapForSort, sortStep + 1);
			OddEvenMergeSort(startPos + half, half, comparatorMapForSort, sortStep + 1);
				
			if (comparatorMapForSort.count(sortStep) == 0)
			{
				comparatorMapForSort.insert(std::make_pair(sortStep, ComparatorMapForMerge()));
			}
			OddEvenMerge(startPos, len, 1, comparatorMapForSort[sortStep], 0);
		}
	}

	void CompareSwap(ValueType &a1, ValueType &a2)
	{
		if (a1 > a2)
		{
			std::swap(a1, a2);
		}
	}

	void InsertIntoComparatorMapForMerge(ComparatorMapForMerge &comparatorMap, int key, Comparator comparator)
	{
		if (comparatorMap.count(key) == 0)
		{
			comparatorMap.insert(std::make_pair(key, ComparatorList(1, comparator)));
		}
		else
		{
			comparatorMap[key].push_back(comparator);
		}
	}

	TactToComparatorsMap CreateTactToComparatorsMap(const ComparatorMapForSort &comparatorMapForSort)
	{
		TactToComparatorsMap tactToComparatorsMap;
		int tact = 0;
		for (auto mapForSortIt = comparatorMapForSort.begin(); mapForSortIt != comparatorMapForSort.end(); ++mapForSortIt)
		{
			std::for_each(mapForSortIt->second.begin(), mapForSortIt->second.end(), [&tact, &tactToComparatorsMap](OddEvenMergeSorter::ComparatorMapForMerge::value_type elem)
			{
				tactToComparatorsMap.insert(std::make_pair(tact, elem.second));
				++tact;
			});
		}
		return tactToComparatorsMap;
	}

private:		
	std::vector<ValueType> &m_vec;
};

#endif // __ODD_EVEN_MERGE_SORTER_H__
