#ifndef __INPUT_PROCESSING_H__
#define __INPUT_PROCESSING_H__

#include "network_generation.h"
#include "validation.h"

#include <string>
#include <vector>

struct TimeStatistics
{
	TimeStatistics() :
		startTime(0),
		endTime(0),
		readStartTime(0),
		readEndTime(0),
		sortStartTime(0),
		sortEndTime(0)
	{
	}
	seconds_t startTime;
	seconds_t endTime;
	seconds_t readStartTime;
	seconds_t readEndTime;
	seconds_t sortStartTime;
	seconds_t sortEndTime;
};

void ReadArray(std::vector<unsigned int> &resultArray, const std::string &filePath);
void WriteArray(const std::vector<unsigned int> &array, const std::string &filePath);

void WriteStatistics(int commSize, std::size_t arraySize,
	const OddEvenMergeSorter::TactToComparatorsMap &tactToComparatorsMap,
	const validation::ArrayValidationInfo &initialArrayInfo,
	const validation::ArrayValidationInfo &sortedArayInfo,
	const TimeStatistics &timeStatistics,
	const std::string &filePath);

#endif // __INPUT_PROCESSING_H__
