#ifndef __ARRAY_VALIDATION_H__
#define __ARRAY_VALIDATION_H__

#include "common.h"
#include "array_info_helpers.h"

#include <vector>
#include <iterator>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>

#include <mpi.h>

namespace validation
{

struct ArrayValidationInfo
{
	ArrayValidationInfo() : sumFullArrayModPower32(0)
	{
	}

	unsigned long long sumFullArrayModPower32;
	std::vector<unsigned long long> sumArrayPartsModPower32;
	std::vector<unsigned int> numberOfElementsModPower16;
};

void FillInArrayValidationInfo(const std::vector<unsigned int> &fullArray, int commSize, ArrayValidationInfo &arrayInfo);

struct IValidator
{
	virtual ~IValidator() = 0;
	virtual void ValidateProcessorState(rank_t rank, const std::vector<unsigned int> &arrayPart) = 0;
	virtual void ValidateProcessorPairState(rank_t ownRank, const std::vector<unsigned int> &arrayPart, rank_t partnerRank) = 0;
	virtual void ValidateControlSums(const ArrayValidationInfo &initialArrayInfo, const ArrayValidationInfo &sortedArayInfo) = 0;
};

struct NoValidator : IValidator
{
	virtual void ValidateProcessorState(rank_t rank, const std::vector<unsigned int> &arrayPart) { }
	virtual void ValidateProcessorPairState(rank_t ownRank, const std::vector<unsigned int> &arrayPart, rank_t partnerRank) { }
	virtual void ValidateControlSums(const ArrayValidationInfo &initialArrayInfo, const ArrayValidationInfo &sortedArayInfo) { }
};

struct Validator : IValidator
{
	virtual void ValidateProcessorState(rank_t rank, const std::vector<unsigned int> &arrayPart);
	virtual void ValidateProcessorPairState(rank_t ownRank, const std::vector<unsigned int> &arrayPart, rank_t partnerRank);
	virtual void ValidateControlSums(const ArrayValidationInfo &initialArrayInfo, const ArrayValidationInfo &sortedArayInfo);
};

} // namespace validation

#endif // __ARRAY_VALIDATION_H__
