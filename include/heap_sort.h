#ifndef __HEAP_SORT_H__
#define __HEAP_SORT_H__

#include <iterator>

template <class RandomAccessIterator>
void ShiftDown(RandomAccessIterator first, RandomAccessIterator last, RandomAccessIterator shiftee)
{
	RandomAccessIterator i_big;
	RandomAccessIterator c1;
	RandomAccessIterator c2;
	while(shiftee < last)
	{
		i_big = shiftee;
		c1 = std::next(shiftee, std::distance(first, shiftee) + 1);
		c2 = std::next(c1, 1);
		if( c1 < last && *c1 > *i_big)
			i_big = c1;
		if( c2 < last && *c2 > *i_big)
			i_big = c2;
		if(i_big == shiftee) return;
		std::swap(*shiftee, *i_big);
		shiftee = i_big;
	}
}

template <class RandomAccessIterator>
void ToHeap(RandomAccessIterator first, RandomAccessIterator last)
{
	RandomAccessIterator i = std::next(first, std::distance(first, last)/2 - 1);
	while(i >= first)
	{
		ShiftDown(first, last, i);
		--i;
	}
}

template <class RandomAccessIterator>
void HeapSort(RandomAccessIterator first, RandomAccessIterator last)
{
	ToHeap(first, last);
	RandomAccessIterator end = std::next(first, std::distance(first, last) - 1);
	while (end != first)
	{
		std::swap(*first, *end);
		ShiftDown(first, end, first);
		--end;
	}
}

#endif // __HEAP_SORT_H__
