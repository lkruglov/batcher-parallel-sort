#ifndef __MPI_HELPERS_H__
#define __MPI_HELPERS_H__

#include <iostream>
#include <sstream>
#include <stdexcept>

#ifndef NDEBUG 
#define DUMP_ERROR_LOCATION() \
	{ \
		std::stringstream msg; \
		msg << "ERROR at " << __FILE__ << ":" << __LINE__ << std::endl; \
		std::cerr << msg.str(); \
	}
#else 
#define DUMP_ERROR_LOCATION()
#endif

typedef int rank_t;
typedef double seconds_t;

struct MPIException: std::runtime_error
{
	MPIException(int rc, const std::string &whatArg) : std::runtime_error(whatArg), m_rc(rc)
	{
	}
	int m_rc;
};

#define MPI_CHECK_SUCCEEDED(rc) \
	if ((rc) != MPI_SUCCESS) \
	{ \
		DUMP_ERROR_LOCATION(); \
		throw MPIException(rc, "Error code = " + std::to_string(rc)); \
	}

#define MPI_CHECK_SUCCEEDED_RETURN(rc) \
	if ((rc) != MPI_SUCCESS) \
	{ \
		DUMP_ERROR_LOCATION(); \
		return (rc); \
	}

#define CHECK(rc, msg) \
	if (!(rc)) \
	{ \
		DUMP_ERROR_LOCATION(); \
		throw std::runtime_error(msg); \
	}

#endif // __MPI_HELPERS_H__
