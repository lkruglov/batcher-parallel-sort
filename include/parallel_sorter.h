#ifndef __PARALLEL_SORTER_H__
#define __PARALLEL_SORTER_H__

#include "common.h"
#include "network_generation.h"
#include "validation.h"

#include <vector>
#include <memory>

#include <mpi.h>

class OddEvenMergeParallelRootSorter
{
public:
	OddEvenMergeParallelRootSorter(rank_t root,
		MPI_Comm communicator,
		std::vector<unsigned int> &fullArrayForRoot,
		const std::shared_ptr<validation::IValidator> &validator);

	void Sort();

	OddEvenMergeSorter::TactToComparatorsMap GetTactToComparatorsMap() const
	{
		return m_tactToComparatorsMap;
	}

private:
	void InitializeArrayPart();
	void CollectResultArray();
	void PerformMerge(rank_t partnerRank, const std::vector<unsigned int> &partnerArrayPart);

	bool IsRoot() const
	{
		return m_rank == m_root;
	}

private:
	class PerProcessSortingNetwork
	{
	public:
		PerProcessSortingNetwork(rank_t rank, const OddEvenMergeSorter::TactToComparatorsMap &tactToComparatorsMap) : 
			m_tactToPartnerMap(GetPartnerForEachTact(rank, tactToComparatorsMap)),
			m_nextTact(0)
		{		
		}

		bool GetPartnerForNextTact(rank_t &partnerRank)
		{
			if (m_tactToPartnerMap.count(m_nextTact) == 0)
			{
				return false;
			}

			partnerRank = m_tactToPartnerMap[m_nextTact];
			++m_nextTact;

			return true;
		}

	private:
		TactToPartnerMap m_tactToPartnerMap;
		std::size_t m_nextTact;
	};

private:
	rank_t m_root;
	rank_t m_rank;
	MPI_Comm m_communicator;
	int m_commSize;

	std::vector<unsigned int> &m_initialArrayRef;

	std::vector<unsigned int> m_fullArrayForRoot;
	std::vector<unsigned int> m_arrayPart;
	std::size_t m_elementsPerProcess;
	std::size_t m_numberOfFakeZeroesForRoot;

	OddEvenMergeSorter::TactToComparatorsMap m_tactToComparatorsMap;
	std::unique_ptr<PerProcessSortingNetwork> m_perProcessSortingNetwork;

	std::shared_ptr<validation::IValidator> m_validator;
};

#endif // __PARALLEL_SORTER_H__
