#ifndef __COMMAND_LINE_PARSER_H__
#define __COMMAND_LINE_PARSER_H__

#include "common.h"

#include <string>
#include <algorithm>

class CommandLineParser
{

public:
	struct Options
	{
		Options() : root(0), disableValidation(false)
		{
		}

		rank_t root;
		bool disableValidation;
		std::string inputFilePath;
		std::string statisticsFilePath;
		std::string outputFilePath;
	};

public:
	CommandLineParser(int argc, const char * const *argv) :
		m_argc(argc),
		m_argv(argv)
	{
	}

	Options Parse()
	{
		Options result;
		result.disableValidation = HasCommandLineStringArg("--disable-validation");
		result.root = std::atoi(GetCommandLineStringArg("--root=").c_str());
		result.inputFilePath = GetCommandLineStringArg("--input=");
		result.statisticsFilePath = GetCommandLineStringArg("--statistics-output="); 
		result.outputFilePath = GetCommandLineStringArg("--output=");		
		return result;
	}

private:
	std::string GetCommandLineStringArg(const std::string &pattern)
	{
		std::string result;
		auto argvEnd = m_argv + m_argc;
		auto findIter = std::find_if(m_argv, argvEnd, [&pattern, &result](const char * const arg)
		{
			const std::string &argStr(arg);
			const std::size_t patternPosition = argStr.find(pattern);
			if (patternPosition != std::string::npos)
			{
				result = argStr.substr(pattern.size());
				return true;
			}
			return false;
		});
		return result;
	}

	bool HasCommandLineStringArg(const std::string &pattern)
	{
		auto argvEnd = m_argv + m_argc;
		auto findIter = std::find_if(m_argv, argvEnd, [&pattern](const char * const arg)
		{
			return pattern == arg;
		});
		return findIter != argvEnd;
	}

private:
	const int m_argc;
	const char * const *m_argv;
};

#endif // __COMMAND_LINE_PARSER_H__
