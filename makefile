default: build

SOURCE = source/main.cpp source/merge_arrays.cpp source/io_processing.cpp source/parallel_sorter.cpp source/validation.cpp
INCLUDE_DIR = include

COMPILER_ARGS_COMMON = -std=c++0x -I $(INCLUDE_DIR) -o batcher_sort $(SOURCE)

build:
	mpic++ -DNDEBUG -O2 $(COMPILER_ARGS_COMMON)

debug_build:
	mpic++ $(COMPILER_ARGS_COMMON)

sample_run:
	mpirun -np 5 batcher_sort --input=samples/small_array.txt --output=out.txt --statistics-output=stats.txt --root=0

all: debug_build sample_run

test:
	python batcher_sort_test.py -p batcher_sort
